/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.zoneboard.common;

import java.awt.Component;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.apertum.qsystem.common.QConfig;
import ru.apertum.qsystem.common.QLog;
import ru.apertum.qsystem.common.QModule;

/**
 *
 * @author egorov
 */
public class Uses {

     /**
     *  Собственно, логер лог4Ж
     */
    public final static class Log extends Object {

        public Logger logger = LogManager.getLogger(QModule.zone_board);//**.file.info.trace
    };
    /**
     * Пользуемся этой константой для работы с логом
     */
    public static final Log LOG = new Log();

    /**
     * Определение политики логирования.
     * @param args параметры командной строки
     * @return вести отладку или нет.
     */
    public static boolean setLogining(String[] args) {
        boolean isDebugin = QConfig.cfg().prepareCLI(QModule.zone_board, args).isDebug();
        Uses.LOG.logger = QLog.log();
        Uses.LOG.logger.info("СТАРТ ЛОГИРОВАНИЯ. Логгер: " + Uses.LOG.logger.getName());
        return isDebugin;
    }

    /**
     * Этот класс исключения использовать для програмной генерации исклюсений.
     * Записывает StackTrace и  само исключение в лог.
     * Это исключение не показывает диологовое окно при возникновении ошибки
     * Используется в системе статистики и отчетов.
     * @author egorov
     */
    public static class SilentException extends RuntimeException {

        public SilentException(String textException) {
            super(textException);
            //StringWriter out = new StringWriter();
            //printStackTrace(new PrintWriter(out));
            //log.logger.error("Error!\n"+out.toString(), this);
            LOG.logger.error("Error!");
        }
    }

    /**
     * Для чтения байт из потока.
     * @param stream из него читаем
     * @return byte[] результат
     * @throws java.io.IOException
     * @see readSocketInputStream(InputStream stream)
     */
    public static byte[] readInputStream(InputStream stream) throws IOException {
        final byte[] result;
        final DataInputStream dis = new DataInputStream(stream);
        result = new byte[stream.available()];
        dis.readFully(result);
        return result;
    }

    /**
     * Отцентирируем Окно по центру экрана
     * @param component это окно и будем центрировать
     */
    public static void setLocation(Component component) {
        final Toolkit kit = Toolkit.getDefaultToolkit();
        component.setLocation((Math.round(kit.getScreenSize().width - component.getWidth()) / 2), (Math.round(kit.getScreenSize().height - component.getHeight()) / 2));
    }
    public static final HashMap<Integer, Rectangle> DISPLAYS = new HashMap<>();

    static {
        final GraphicsDevice[] screenDevices = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
        int i = 1;
        for (GraphicsDevice graphicsDevice : screenDevices) {
            System.out.println("graphicsDevice = " + graphicsDevice.getIDstring() + " " + graphicsDevice.toString()
                    + "\nРазрешение экрана " + graphicsDevice.getDefaultConfiguration().getBounds().height + "x" + graphicsDevice.getDefaultConfiguration().getBounds().width
                    + "\nГлубина цвета " + graphicsDevice.getDisplayMode().getBitDepth()
                    + "\nЧастота " + graphicsDevice.getDisplayMode().getRefreshRate()
                    + "\nНачало координат " + graphicsDevice.getDefaultConfiguration().getBounds().x
                    + "-" + graphicsDevice.getDefaultConfiguration().getBounds().y);
            DISPLAYS.put(i++, graphicsDevice.getDefaultConfiguration().getBounds());
        }

    }
    
    

}
