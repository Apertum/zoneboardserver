/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.zoneboard;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Properties;
import javax.imageio.ImageIO;
import javax.xml.bind.JAXBException;
import ru.apertum.qsystem.client.forms.FIndicatorBoard;
import ru.apertum.qsystem.client.forms.FIndicatorBoargJaxb;
import ru.apertum.qsystem.common.CustomerState;
import ru.apertum.qsystem.common.QConfig;
import ru.apertum.qsystem.common.exceptions.ServerException;
import ru.apertum.qsystem.common.model.QCustomer;
import ru.apertum.qsystem.server.board.Board;
import ru.apertum.qsystem.server.controller.AIndicatorBoard;
import ru.apertum.qsystem.server.controller.QIndicatorBoardMonitor;
import ru.apertum.qsystem.server.model.QUser;

/**
 *
 * @author Evgeniy Egorov
 */
public class ZoneBoard extends QIndicatorBoardMonitor {

    final private ZoneServerProperty props;

    public final Board getConfig(String fileName) {
        final String configFile1;
        if (new File(fileName).exists()) {
            configFile1 = fileName;
        } else {
            configFile1 = props.getConfigFile();
        }
        try {
            return Board.unmarshal(new File(configFile1));
        } catch (FileNotFoundException | JAXBException ex) {
            throw new ServerException("Не создали клиентское табло.", ex);
        }
    }

    /**
     *
     * @param props
     * @param p
     */
    public ZoneBoard(ZoneServerProperty props, WindowProperty p) {
        this.props = props;
        final FIndicatorBoard w = FIndicatorBoargJaxb.getIndicatorBoardForZone(getConfig(p.getConfigFile()), QConfig.cfg().isDebug());
        try {
            w.setIconImage(ImageIO.read(ZoneBoard.class.getResource("/ru/apertum/qsystem/zoneboard/form/resources/zrecent.png")));
        } catch (IOException ex) {
            System.err.println(ex);
        }
        final Properties prop = new Properties();
        try {
            prop.load(ZoneBoard.class.getResourceAsStream("/zoneboardserver.properties"));
        } catch (IOException ex) {
            System.err.println(ex);
        }
        w.setTitle(w.getTitle() + "   Zone boards ver." + prop.getProperty("version") + "_" + prop.getProperty("date"));
        setForm(w);
    }

    public final void setForm(FIndicatorBoard form) {
        indicatorBoard = form;
        initIndicatorBoard();
    }

    @Override
    protected void initIndicatorBoard() {
        setPause(indicatorBoard.getPause());
        if (!getRecords().isEmpty()) {
            showOnBoard(new LinkedList<>(getRecords().values()));
        }
    }

    //**************************************************************************
    //************************** Методы взаимодействия *************************
    @Override
    public synchronized void inviteCustomer(QUser user, QCustomer customer) {
        //super.inviteCustomer(user, customer); 
        Record rec = getRecords().get(user.getName());
        if (rec == null) {
            rec = new AIndicatorBoard.Record(user.getName(), user.getPoint(), customer.getPrefix(), customer.getNumber(), "", user.getAdressRS(), getPause());
        } else {
            addItem(rec);
        }
        show(rec);

        // механизм панели вызова конкретного номерка
        if (indicatorBoard != null) {
            indicatorBoard.showCallPanel(customer.getPrefix() + customer.getNumber(), user.getPoint());
        }
    }

    @Override
    public synchronized void workCustomer(QUser user) {
        Record rec = getRecords().get(user.getName());
        //запись может быть не найдена после рестарта сервера, список номеров на табло не бакапится
        if (rec == null) {
            rec = new AIndicatorBoard.Record(user.getName(), user.getPoint(), user.getCustomer().getPrefix(), user.getCustomer().getNumber(), "", user.getAdressRS(), getPause());
        }
        rec.setState(CustomerState.STATE_WORK);
        show(rec);
    }

    @Override
    public synchronized void killCustomer(QUser user) {
        final Record rec = getRecords().get(user.getName());
        //запись может быть не найдена после рестарта сервера, список номеров на табло не бакапится
        if (rec != null) {
            rec.setState(CustomerState.STATE_DEAD);
            removeItem(rec);
            show(rec);
        }
    }
}
