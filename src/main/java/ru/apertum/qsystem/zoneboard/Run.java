/**
 * To change this template, choose Tools | Templates and open the template in the editor.
 */
package ru.apertum.qsystem.zoneboard;

import com.google.gson.Gson;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.MemoryImageSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.ServiceLoader;
import javax.swing.JFrame;
import javax.xml.bind.JAXBException;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import ru.apertum.qsystem.client.Locales;
import ru.apertum.qsystem.common.CustomerState;
import ru.apertum.qsystem.common.QConfig;
import ru.apertum.qsystem.common.QLog;
import ru.apertum.qsystem.common.QModule;
import ru.apertum.qsystem.common.SoundPlayer;
import ru.apertum.qsystem.common.cmd.JsonRPC20Error;
import ru.apertum.qsystem.common.cmd.JsonRPC20OK;
import ru.apertum.qsystem.common.cmd.RpcGetInt;
import ru.apertum.qsystem.common.cmd.RpcToZoneServer;
import ru.apertum.qsystem.common.model.QCustomer;
import ru.apertum.qsystem.extra.IChangeCustomerStateEvent;
import ru.apertum.qsystem.server.controller.IIndicatorBoard;
import ru.apertum.qsystem.server.model.QService;
import ru.apertum.qsystem.server.model.QUser;
import ru.apertum.qsystem.zoneboard.common.Uses;
import ru.apertum.qsystem.zoneboard.form.FAdv;
import ru.apertum.qsystem.zoneboard.form.FZoneBoard;

/**
 *
 * @author egorov
 */
public class Run {

    static private ZoneServerProperty props;
    static public String filePropName;
    private static final ArrayList<FZoneBoard> FORMS = new ArrayList<>();
    public static final HashMap<Integer, ArrayList<IIndicatorBoard>> BOARDS = new HashMap<>();

    /**
     * поддержка расширяемости плагинами
     *
     * @return Табло из плагина
     */
    private static IIndicatorBoard getPlugTablo() {
        IIndicatorBoard res = null;
        for (final IIndicatorBoard board : ServiceLoader.load(IIndicatorBoard.class)) {
            QLog.l().logger().info("Вызов SPI расширения. Описание: " + board.getDescription());
            return res;
        }
        return null;
    }

    public static void main(final String args[]) throws FileNotFoundException, SocketException, IOException {
        QLog.initial(args, QModule.zone_board);
        Uses.setLogining(args);
        Locale.setDefault(Locales.getInstance().getLangCurrent());
        // Загрузка плагинов из папки plugins
        ru.apertum.qsystem.common.Uses.loadPlugins("./plugins/");
        final InputStream insProp;
        File f = new File(QConfig.cfg().getZoneBoardCfgFile());
        if (f.exists()) {
            insProp = new FileInputStream(f);
            filePropName = QConfig.cfg().getZoneBoardCfgFile();
        } else {
            QLog.l().logger().warn("Config file \"" + QConfig.cfg().getZoneBoardCfgFile() + "\" not exists.");
            f = new File("config/config.xml");
            if (f.exists()) {
                insProp = new FileInputStream(f);
                filePropName = "config/config.xml";
            } else {
                throw new FileNotFoundException("Not found the configuration file config/config.xml in current directory.");
            }
        }
        try {
            props = ZoneServerProperty.unmarshal(insProp);
        } catch (JAXBException ex) {
            throw new Uses.SilentException(ex.toString());
        }

        // Окна
        props.windows.stream().filter((p) -> !(!Uses.DISPLAYS.containsKey(p.getDisplay()))).forEachOrdered((p) -> {
            java.awt.EventQueue.invokeLater(() -> {
                //final FZoneBoard w = new FZoneBoard(p);
                //final FIndicatorBoard w = FIndicatorBoard.getIndicatorBoardForZone(getConfig(p.getConfigFile() == null ? "" : p.getConfigFile()), Uses.isDebug);
                final IIndicatorBoard res = getPlugTablo();
                final IIndicatorBoard board;
                if (res == null) {
                    board = new ZoneBoard(props, p);
                } else {
                    board = res;
                }
                //board.setForm(w);
                p.getZones().forEach((n) -> {
                    synchronized (BOARDS) {
                        ArrayList<IIndicatorBoard> list = BOARDS.get(n);
                        if (list == null) {
                            list = new ArrayList<>();
                            BOARDS.put(n, list);
                        }
                        list.add(board);
                    }
                });
                if (board.getBoardForm() != null) {
                    int x, y;
                    if (QConfig.cfg().isDebug() && !QConfig.cfg().isDemo()) {
                        int k1 = getK();
                        x = 100 * k1;
                        y = 100 * k1;
                    } else {
                        x = (int) Uses.DISPLAYS.get(p.getDisplay()).getX() + 10;
                        y = (int) Uses.DISPLAYS.get(p.getDisplay()).getY() + 10;
                    }
                    if (board.getBoardForm() instanceof JFrame) {
                        toPosition(((JFrame) board.getBoardForm()), QConfig.cfg().isDebug(), x, y);
                        ((JFrame) board.getBoardForm()).setVisible(true);
                    }
                }
            });
        });

        // Окна с рекламой
        props.getAdvWindows().stream().filter((p) -> !(!Uses.DISPLAYS.containsKey(p.getDisplay()))).forEachOrdered((p) -> {
            java.awt.EventQueue.invokeLater(() -> {
                final FAdv adv = new FAdv(p, QConfig.cfg().isDebug());
                int x, y;
                if (QConfig.cfg().isDebug()) {
                    int k1 = getK();
                    x = 100 * k1;
                    y = 100 * k1;
                } else {
                    x = (int) Uses.DISPLAYS.get(p.getDisplay()).getX();
                    y = (int) Uses.DISPLAYS.get(p.getDisplay()).getY();
                }
                adv.setLocation(x, y);
                adv.setVisible(true);
            });
        });
        // привинтить сокет на локалхост
        //  запуска захвата портов
        new Thread(new SocketRunnable(props.getPort())).start();

    }
    static int k = 1;

    public static int getK() {
        return k++;
    }

    public static void toPosition(final JFrame form, boolean isDebug, int x, int y) {
        // Определим форму нв монитор
        form.setLocation(x, y);
        form.setAlwaysOnTop(!isDebug);
        //form.setResizable(isDebug);
        // Отрехтуем форму в зависимости от режима.
        if (!isDebug) {

            form.setAlwaysOnTop(true);
            //form.setResizable(false);
            // спрячем курсор мыши
            int[] pixels = new int[16 * 16];
            Image image = Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(16, 16, pixels, 0, 16));
            Cursor transparentCursor = Toolkit.getDefaultToolkit().createCustomCursor(image, new Point(0, 0), "invisibleCursor");
            form.setCursor(transparentCursor);
            form.addWindowListener(new WindowAdapter() {

                @Override
                public void windowOpened(WindowEvent e) {
                    form.setExtendedState(JFrame.MAXIMIZED_BOTH);
                }
            });
        } else {
            form.setSize(1280, 720);
        }
    }

    public static Element getConfig(String fileName) {
        final String configFile;
        if (new File(fileName).exists()) {
            configFile = fileName;
        } else {
            configFile = props.getConfigFile();
        }
        final File boardFile = new File(configFile);
        if (boardFile.exists()) {
            try {
                return new SAXReader(false).read(boardFile).getRootElement();
            } catch (DocumentException ex) {
                Uses.LOG.logger.error("Невозможно прочитать файл конфигурации главного табло. " + ex.getMessage());
                return DocumentHelper.createElement("Ответ");
            }
        } else {
            Uses.LOG.logger.warn("Файл конфигурации главного табло \"" + configFile + "\" не найден. ");
            return DocumentHelper.createElement("Ответ");
        }
    }

    static public void refreshWindowsTitle() {
        FORMS.forEach((form) -> {
            StringBuilder caption = new StringBuilder("    ");
            form.getProperty().getZones().forEach((port) -> {
                props.getZoneList().stream().filter((pos) -> (pos.getZone().equals(port))).forEach((pos) -> {
                    caption.append(pos.getName()).append("(").append(pos.getZone()).append("); ");
                });
            });
            form.setTitle(form.getProperty().getName() + caption.toString());
        });

    }

    static class SocketRunnable implements Runnable {

        private final Integer port;

        public SocketRunnable(Integer port) {
            this.port = port;
        }

        @Override
        public void run() {

            final ServerSocket server;
            try {
                Uses.LOG.logger.info("Сервер системы захватывает порт \"" + port + "\".");
                server = new ServerSocket(port);
            } catch (IOException e) {
                throw new Uses.SilentException("Ошибка при создании серверного сокета: " + e);
            } catch (Exception e) {
                throw new Uses.SilentException("Ошибка сети: " + e);
            }
            Uses.LOG.logger.debug("Старт цикла приема сообщений.");
            // слушаем порт
            while (!Thread.interrupted()) {
                // из сокета клиента берём поток входящих данных
                final Socket socket;
                try {
                    socket = server.accept();
                } catch (IOException e) {
                    throw new Uses.SilentException("Ошибка при получении входного потока: " + e);
                }
                final InputStream inR;
                try {
                    inR = socket.getInputStream();
                } catch (IOException ex) {
                    throw new Uses.SilentException("Ошибка сети 1: " + ex);
                }

                final String data;
                try {
                    // подождать пока хоть что-то приползет из сети, но не более 10 сек.
                    int i = 0;
                    while (inR.available() == 0 && i < 100) {
                        Thread.sleep(100);//бля
                        i++;
                    }

                    StringBuilder sb1 = new StringBuilder(new String(Uses.readInputStream(inR)));
                    while (inR.available() != 0) {
                        sb1 = sb1.append(new String(Uses.readInputStream(inR)));
                        Thread.sleep(150);//бля
                    }
                    data = URLDecoder.decode(sb1.toString(), "utf-8");
                } catch (IOException ex) {
                    throw new Uses.SilentException("Ошибка при чтении из входного потока: " + ex);
                } catch (InterruptedException ex) {
                    throw new Uses.SilentException("Проблема со сном: " + ex);
                } catch (IllegalArgumentException ex) {
                    throw new Uses.SilentException("Ошибка декодирования сетевого сообщения: " + ex);
                }

                Uses.LOG.logger.trace("Сообщение:\n" + data);

                final RpcToZoneServer rpc;
                final Gson gson = new Gson();

                rpc = gson.fromJson(data, RpcToZoneServer.class);
                try {
                    new Thread(new WorkRunnable(rpc, socket.getOutputStream())).start();
                } catch (IOException ex) {
                    throw new Uses.SilentException("Ошибка обработки и отправки сетевого сообщения: " + ex);
                }

            }

        }
    }

    private static final HashMap<String, Long> REPEAT = new HashMap<>();

    private static synchronized HashMap<String, Long> getR() {
        if (REPEAT.size() > 1000) {
            HashSet<String> del = new HashSet<>();
            REPEAT.keySet().stream().filter((key) -> (System.currentTimeMillis() - REPEAT.get(key) > 1000 * 60 * 3)).forEach((key) -> {
                del.add(key);
            });
            del.stream().forEach((del1) -> {
                REPEAT.remove(del1);
            });
        }
        return REPEAT;
    }

    static class WorkRunnable implements Runnable {

        public final RpcToZoneServer rpc;
        public final OutputStream os;

        public WorkRunnable(RpcToZoneServer rpc, OutputStream os) {
            this.rpc = rpc;
            this.os = os;
        }

        @Override
        public void run() {
            Uses.LOG.logger.trace("Выполняем метод: \"" + rpc.getMethod());
            System.out.print("Run method: " + rpc.getMethod() + "  parameter: ");
            final Object ansver;
            final ArrayList<IIndicatorBoard> list = rpc.getResult() == null ? null : BOARDS.get(rpc.getResult().getUserAddrRS());
            if (list != null) {
                final QUser user = new QUser();
                user.setName(rpc.getResult().getUserName());
                user.setPoint(rpc.getResult().getUserPoint());
                user.setAdressRS(rpc.getResult().getUserAddrRS());
                final QCustomer customer = new QCustomer();
                customer.setPrefix(rpc.getResult().getCustomerPrefix());
                customer.setNumber(rpc.getResult().getCustomerNumber());
                final QService service = new QService();
                service.setId(Long.MIN_VALUE);
                if (rpc.getResult().getSoundTemplate() == null) {

                    service.setSoundTemplate("1" + props.getInviteType() + "11" + props.getPointType() + "1");
                } else {
                    service.setSoundTemplate(rpc.getResult().getSoundTemplate());
                }
                customer.setService(service);
                user.setCustomer(customer);

                switch (rpc.getMethod()) {
                    case "ping":
                        final int res;
                        System.out.println(rpc.getParams().textData);
                        switch (rpc.getParams().textData) {
                            case "2":
                                res = 1;
                                break;
                            default:
                                res = -300;
                        }
                        ansver = new RpcGetInt(res);
                        break;
                    case "show":
                        System.out.println(rpc.getResult().getUserAddrRS());
                        list.stream().map((board) -> {
                            System.out.println("do show");
                            return board;
                        }).forEachOrdered((board) -> {
                            board.inviteCustomer(user, customer);
                        });
                        // выясним повторно зовут или нет
                        String key = rpc.getResult().getCustomerPrefix() + rpc.getResult().getCustomerNumber() + rpc.getResult().getUserPoint();
                        Long t = getR().get(key);
                        boolean isFirst = true;
                        if (t != null) {
                            isFirst = System.currentTimeMillis() - t > 1000 * 60 * 3;
                        }
                        getR().put(key, System.currentTimeMillis());
                        // просигналим звуком
                        SoundPlayer.inviteClient(service, customer, customer.getSemiNumber(), user.getPoint(), isFirst);
                        ansver = new JsonRPC20OK();
                        break;
                    case "work":
                        System.out.println(rpc.getResult().getUserAddrRS());
                        list.stream().map((board) -> {
                            System.out.println("do work");
                            return board;
                        }).forEachOrdered((board) -> {
                            board.workCustomer(user);
                        });
                        ansver = new JsonRPC20OK();
                        break;
                    case "kill":
                        System.out.println(rpc.getResult().getUserAddrRS());
                        list.stream().map((board) -> {
                            System.out.println("do kill");
                            return board;
                        }).forEachOrdered((board) -> {
                            board.killCustomer(user);
                        });
                        ansver = new JsonRPC20OK();
                        break;
                    default:
                        System.out.println("Warning: default nethod");
                        ansver = new JsonRPC20Error(JsonRPC20Error.ErrorMsg.UNKNOWN_ERROR, "Warning: default nethod");
                }
            } else {
                System.out.println("do nothing. no zone");
                ansver = new JsonRPC20OK();
            }

            CustomerState cs = CustomerState.STATE_FINISH;
            switch (rpc.getMethod()) {
                case "ping":
                    break;
                case "show":
                    cs = CustomerState.STATE_INVITED;
                    break;
                case "repeat":
                    break;
                case "work":
                    cs = CustomerState.STATE_WORK;
                    break;
                case "kill":
                    break;
                default:
                    System.out.println("Warning: default nethod");
            }
            // поддержка расширяемости плагинами
            for (final IChangeCustomerStateEvent event : ServiceLoader.load(IChangeCustomerStateEvent.class)) {
                System.out.println("Call SPI extention. Description: " + event.getDescription());
                try {
                    event.change(rpc.getResult().getUserPoint(), rpc.getResult().getCustomerPrefix(), rpc.getResult().getCustomerNumber(), cs);
                } catch (Throwable tr) {
                    System.err.println("Calling SPI extention was breken. Description: " + tr);
                }
            }

            try (PrintWriter writer = new PrintWriter(os)) {
                try {
                    Gson gson = new Gson();
                    final String message = gson.toJson(ansver);
                    Uses.LOG.logger.trace("Высылаем результат: \"" + message + "\"");
                    writer.print(URLEncoder.encode(message, "utf-8"));
                } catch (UnsupportedEncodingException ex) {
                    throw new Uses.SilentException("Ошибка отправки сетевого сообщения: " + ex);
                }
                Uses.LOG.logger.trace("Высылали результат.");
                writer.flush();
            }
        }
    }
}
