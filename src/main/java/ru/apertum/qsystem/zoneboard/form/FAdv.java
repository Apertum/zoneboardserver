/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

 /*
 * FAdv.java
 *
 * Created on 01.07.2010, 16:01:31
 */
package ru.apertum.qsystem.zoneboard.form;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import ru.apertum.qsystem.client.model.QPanel;
import ru.apertum.qsystem.common.QConfig;
import ru.apertum.qsystem.zoneboard.AdvWindowProperty;

/**
 *
 * @author egorov
 */
public class FAdv extends javax.swing.JFrame {
    
    final private AdvWindowProperty property;
    
    public AdvWindowProperty getProperty() {
        return property;
    }

    /**
     * Creates new form FGrid
     *
     * @param property
     * @param isDebug
     */
    public FAdv(AdvWindowProperty property, boolean isDebug) {
        initComponents();
        this.property = property;
        if (Files.isDirectory(Paths.get(property.getFilePath()))) {
            startVideo(property.getFilePath());
        } else {
            labelImage.setIcon(new ImageIcon(property.getFilePath()));
        }
        if (!QConfig.cfg().isDebug()) {
            addWindowListener(new WindowAdapter() {
                
                @Override
                public void windowOpened(WindowEvent e) {
                    setExtendedState(JFrame.MAXIMIZED_BOTH);
                }
            });
        }
    }
    
    private final void startVideo(String folder) {
        QPanel panel = new QPanel();
        basePanel.remove(labelImage);
        basePanel.add(panel);
        panel.setVideoFileName(folder);
        panel.startVideo();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        basePanel = new javax.swing.JPanel();
        labelImage = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        basePanel.setBackground(new java.awt.Color(255, 255, 51));
        basePanel.setName("basePanel"); // NOI18N
        basePanel.setLayout(new java.awt.GridLayout());

        labelImage.setBackground(new java.awt.Color(255, 255, 255));
        labelImage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelImage.setName("labelImage"); // NOI18N
        labelImage.setOpaque(true);
        basePanel.add(labelImage);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(basePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 661, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(basePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 473, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel basePanel;
    private javax.swing.JLabel labelImage;
    // End of variables declaration//GEN-END:variables
}
